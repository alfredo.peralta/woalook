import { Component, OnInit } from '@angular/core';
import { CargaScriptsService } from 'src/app/carga-scripts.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private _CargaScriptsService:CargaScriptsService) { 
    _CargaScriptsService.Carga(["menu"]);
  }

  ngOnInit(): void {
  }

}
