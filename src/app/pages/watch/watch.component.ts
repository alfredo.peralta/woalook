import { Component, OnInit } from '@angular/core';
import { CargaScriptsService } from 'src/app/carga-scripts.service';

@Component({
  selector: 'app-watch',
  templateUrl: './watch.component.html',
  styleUrls: ['./watch.component.css']
})
export class WatchComponent implements OnInit {

  constructor(private _CargaScriptsService:CargaScriptsService) { 
    _CargaScriptsService.Carga(["reproductor"]);
  }

  ngOnInit(): void {
  }

}
