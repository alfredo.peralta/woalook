import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private URL  = 'http://localhost:8080';

  constructor(private http: HttpClient, private router: Router) { }

  login(user: any){
    return this.http.post<any>(this.URL+"/auth/login.go", user);
  }

}
