import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { WatchComponent } from './pages/watch/watch.component';

const routes: Routes = [
  {path: '', redirectTo: '/', pathMatch: 'full'}, 
  {path: 'login', component: LoginComponent},
  {path: 'watch', component: WatchComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
