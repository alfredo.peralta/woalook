var video = document.getElementsByTagName("video")[0];
var linea = document.getElementById("lineatiempo");
var lineavolumen = document.getElementById("lineavolumen");
var captura = document.getElementsByClassName("captura")[0];
var capturaTiempo = document.getElementsByClassName("captura-tiempo")[0];
var tiempo = document.getElementsByClassName("tiempo")[0];
var btnvolumen = document.getElementsByClassName("btn-volumen")[0];
let volumen = document.getElementsByClassName("volumen")[0];
var btnconfiguracion = document.getElementsByClassName("btn-config")[0];
let configuracion = document.getElementsByClassName("configuracion")[0];
let circlePlayMedia = document.getElementById("play-center-media");
let btnover = document.getElementsByClassName("sublime");
var btnexpandir = document.getElementsByClassName("btn-expandir")[0];
const timeoutvideo = setInterval(accionesVideo, 1000);
const actionvideo = setInterval(accionVideo, 6000);
const detectScreenVideo = setInterval(detectScreen,1000);
let fotograma = video.cloneNode(true);
let reproductor = document.getElementsByClassName("reproductor")[0];
let controles = document.getElementsByClassName("controles")[0];
let subcapa = document.getElementsByClassName("subcapa")[0];
let subtitulos = document.getElementsByTagName("track")[0];
let menuSubtitulos = document.getElementsByClassName("menu-subtitulos")[0];
var cursor = false;
let pantalla = true;
document.body.appendChild(fotograma);
fotograma.style.display = "none";

    var contenido = [];
    
    function iniciardetener(){
        if(video.paused || video.ended){
            reproducir();
        }else{
            detener();
        }
    }

    function adelantar(){
        video.currentTime += 10;
        document.getElementById("arrow-rigth-img").classList.remove("arrow-rigth-img-out");
        document.getElementById("arrow-rigth-img").classList.add("arrow-rigth-img-in");
        document.getElementById("p-arrow-rigth").classList.remove("arrow-rigth-p-out");
        document.getElementById("p-arrow-rigth").classList.add("arrow-rigth-p-in");
        setTimeout(function(){
        document.getElementById("arrow-rigth-img").classList.add("arrow-rigth-img-out");
        document.getElementById("p-arrow-rigth").classList.add("arrow-rigth-p-out");
        },900);
    }

    function retroceder(){
        video.currentTime -= 10;
        document.getElementById("arrow-left-img").classList.remove("arrow-left-img-out");
        document.getElementById("arrow-left-img").classList.add("arrow-left-img-in");
        document.getElementById("p-arrow-left").classList.remove("arrow-left-p-out");
        document.getElementById("p-arrow-left").classList.add("arrow-left-p-in");
        setTimeout(function(){
        document.getElementById("arrow-left-img").classList.add("arrow-left-img-out");
        document.getElementById("p-arrow-left").classList.add("arrow-left-p-out");
        },900);
    }

    function muted(){
        var muted = 1;
        if(video.volume > 0){
            document.getElementsByClassName("volume")[0].setAttribute("src","../../../assets/img/mute.png");
            video.volume = 0;
            lineavolumen.value = 0;
        }else{
            document.getElementsByClassName("volume")[0].setAttribute("src","../../../assets/img/volume.png");
            video.volume = muted;
            lineavolumen.value = 100;
        }
    }

    function velocidad(numero){
        video.playbackRate = numero;
        let velocidad = "";
        if(numero == 1){
            velocidad = "normal";
        }else if(numero == 0.75){
            velocidad = "lenta";
        }else if(numero == 0.25){
            velocidad = "muy lenta";
        }else if(numero == 1.25){
            velocidad = "rápida"
        }else if(numero == 1.5){
            velocidad = "muy rápida";
        }
        let mensaje = "velocidad "+velocidad;
        mostrarAlerta(mensaje);
    }

    function activaSubtitulos(posicion = null,indice = null, idioma = null){ 
        if(posicion != null && indice != null && idioma != null){
            subtitulos.setAttribute("label",contenido[posicion]["subtitulos"][indice]["idioma"]);
            subtitulos.setAttribute("srclang",contenido[posicion]["subtitulos"][indice]["lang"]);
            subtitulos.setAttribute("src",contenido[posicion]["subtitulos"][indice]["url"]);
            mostrarAlerta("Subtítulos activados");
        }else{
            limpiaSubtitulos();
            mostrarAlerta("Subtítulos desactivados");
        }
    }

    function limpiaSubtitulos(){
        subtitulos.setAttribute("label","");
        subtitulos.setAttribute("srclang","");
        subtitulos.setAttribute("src","");
    }

    function mostrarAlerta(mensaje){   
        let respuesta = "<p>"+mensaje+"</p>";
        document.getElementById("alerts").innerHTML = '';
        document.getElementById("alerts").innerHTML = '<div class="alerta-video"></div>';
        document.getElementsByClassName("alerta-video")[0].innerHTML = respuesta;
        document.getElementsByClassName("alerta-video")[0].style.animation = "mostrar-alerta 4s alternate";
    }

    function expandir(){
        if(pantalla){
            btnexpandir.setAttribute("src","../../../assets/img/minimizar.png");
            if(reproductor.requestFullScreen) {
                reproductor.requestFullScreen();
              } else if(reproductor.mozRequestFullScreen) {
                reproductor.mozRequestFullScreen();
              } else if(reproductor.webkitRequestFullScreen) {
                reproductor.webkitRequestFullScreen();
              }
              pantalla = false;
        }else{
            btnexpandir.setAttribute("src","../../../assets/img/expand.png");
            
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
            pantalla = true;
        }
    }

    function detectScreen(){
        /** Volume **/
        if(video.volume == 1 || video.volume >= 0.51){
            document.getElementsByClassName("volume")[0].setAttribute("src","../../../assets/img/volume.png");
        }
        if(video.volume <= 0.50){
            document.getElementsByClassName("volume")[0].setAttribute("src","../../../assets/img/volume-medium.png");
        }
        if(video.volume == 0){
            document.getElementsByClassName("volume")[0].setAttribute("src","../../../assets/img/mute.png");
        }

        /** video ended **/
        if(video.ended){
            document.getElementsByClassName("iniciar")[0].setAttribute("src","../../../assets/img/replay.png");
        }

        /** Size Screen **/
        this.fullScreenMode = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen; // This will return true or false depending on if it's full screen or not.
        if(this.fullScreenMode == false){
            btnexpandir.setAttribute("src","../../../assets/img/expand.png");
            pantalla = true;
        }       
    }

    //Acciones
    function reproducir(){
        document.getElementsByClassName("iniciar")[0].setAttribute("src","../../../assets/img/pausa.png");
        document.getElementById("play-center-img").setAttribute("src","../../../assets/img/play-button-center.png");
        circlePlayMedia.classList.remove("circle-play-center-media-out");
        document.getElementById("play-center-img").classList.remove("play-center-img-effect-out");
        document.getElementById("play-center-img").classList.add("play-center-img-effect-in");
        circlePlayMedia.classList.add("circle-play-center-media-in");
        setTimeout(function(){
        document.getElementById("play-center-img").classList.add("play-center-img-effect-out");
        circlePlayMedia.classList.remove("circle-play-center-media-in");
        circlePlayMedia.classList.add("circle-play-center-media-out");
        },1000);
        video.play();
    }

    function detener(){
        document.getElementsByClassName("iniciar")[0].setAttribute("src","../../../assets/img/play.png");
        if(!video.ended){
        document.getElementById("play-center-img").setAttribute("src","../../../assets/img/pause-center.png");
        circlePlayMedia.classList.remove("circle-play-center-media-out");
        document.getElementById("play-center-img").classList.remove("play-center-img-effect-out");
        document.getElementById("play-center-img").classList.add("play-center-img-effect-in");
        circlePlayMedia.classList.add("circle-play-center-media-in");
        setTimeout(function(){
            document.getElementById("play-center-img").classList.add("play-center-img-effect-out");
            circlePlayMedia.classList.remove("circle-play-center-media-in");
            circlePlayMedia.classList.add("circle-play-center-media-out");
            },1000);
        video.pause();
        }
    }

    function reproducirPor(tiempo){
        video.currentTime = tiempo;        
    }

    function volumenPor(valor){
        video.volume = valor/100;
    }

    linea.addEventListener("change", function(){
        reproducirPor(linea.value);
    });

    lineavolumen.addEventListener("change", function(e){
        volumenPor(e.target.value);
    });

    linea.addEventListener("mousemove", function(e){
        captura.style.display = "block";
        let movimiento =  parseFloat(captura.style.left.replaceAll("px",""));
        
        let valor = ((e.layerX) > 0) ? (e.layerX-55 > 0) ? e.layerX-55 : (e.layerX > 0 && e.layerX == 8) ? e.layerX : 8 : (e.layerX > 0 || e.layerX == 8) ? e.layerX : 8;
        captura.style.left = valor+"px";
        
        canvas = document.getElementById("escena");
        let resultado = calcula(e).toFixed(2);
        fotograma.currentTime = resultado;
        capturaTiempo.innerHTML = "<span>"+segundosMinutos(fotograma.currentTime)+" / "+segundosMinutos(fotograma.duration)+"</span>";
        draw(fotograma,canvas);
    });

    function calcula(e) {
        return (e.offsetX / e.target.clientWidth) *  parseInt(e.target.getAttribute('max'),10);
    }

    function draw(fotograma, canvas) {
        var context = canvas.getContext('2d');
        context.drawImage(fotograma, 0, 0, canvas.width, canvas.height);
    }

    linea.addEventListener("mouseout", function(e){
        captura.style.display = "none";
    });

    function cambioVideo(posicion){
        let ruta = contenido[posicion]["url"];
        let tipo = contenido[posicion]["formato"];
        let videos = document.getElementsByTagName("video");
        let titulo = document.getElementsByClassName("title")[0];
        let descripcion = document.getElementsByClassName("descripcion")[0];
        videos[0].getElementsByTagName("source")[0].setAttribute("src", ruta);
        videos[0].getElementsByTagName("source")[0].setAttribute("type", tipo);
        videos[0].load();
        document.getElementsByClassName("iniciar")[0].setAttribute("src","../../../assets/img/play.png");
        video.setAttribute("poster",contenido[posicion]["portada"]);
        limpiaSubtitulos();
        videos[0].pause();

        titulo.innerHTML = contenido[posicion]["titulo"];
        descripcion.innerHTML = contenido[posicion]["descripcion"];

        videos[1].getElementsByTagName("source")[0].setAttribute("src", ruta);
        videos[1].getElementsByTagName("source")[0].setAttribute("type", tipo);
        videos[1].load();
        document.getElementsByClassName("iniciar")[0].setAttribute("src","../../../assets/img/play.png");
        videos[1].pause();

        crearMenuSubtitulos(posicion);
    }

    function crearMenuSubtitulos(posicion){
        //subtitulos
        let creaSubtitulos = "<li>";
        if(contenido[posicion]["subtitulos"].length > 0){
            creaSubtitulos += `<a onclick='activaSubtitulos()' class='btns-configuracion'>Desactivar</a>`;
            for(var j = 0; j < contenido[posicion]["subtitulos"].length; j++){
                creaSubtitulos += `<a onclick="activaSubtitulos(${posicion},${j},'${contenido[posicion]["subtitulos"][j]["idioma"]}')" class="btns-configuracion">${contenido[posicion]["subtitulos"][j]["idioma"]}</a>`;
                }
            }else{
                creaSubtitulos += "<h1>No hay subtitulos disponibles</h1>";
            }
        creaSubtitulos += "</li>";
        menuSubtitulos.innerHTML = "";
        menuSubtitulos.innerHTML = creaSubtitulos;
        limpiaSubtitulos();
    }

    function segundosMinutos(duracion){//00:00
        var segundos = duracion;
        var minutos = Math.floor(segundos / 60); 
        var seg = Math.round(segundos % 60); 
    
            if (minutos < 10) {
              minutos = "0" + minutos;
            }
    
            if (seg < 10) {
              seg = "0" + seg;
            }
    
            if (seg == 60) {
              minutos = parseInt(minutos) + 1;
              minutos = "0" + minutos;
              seg = "00";
            }
    
            duracionTotal = minutos + ":" + seg;
            return duracionTotal;
      }

    reproductor.addEventListener("mouseover", function(e){
    controles.classList.remove("ocultar");
    controles.classList.add("mostrar");
    });

    function entraaccion(){
        botones = btnover.length;
        for(var i = 0; i < botones; i++){
            btnover[i].addEventListener("mouseover", function(){
                volumen.style.display = "none";
                configuracion.style.display = "none";
            });     
        }
    }

    entraaccion();

    reproductor.addEventListener("mouseout", function(e){
    controles.classList.remove("mostrar");
    controles.classList.add("ocultar");
    });

    btnvolumen.addEventListener("mouseover", function(e){
        volumen.style.display = "block";
        configuracion.style.display = "none";
    });

    btnconfiguracion.addEventListener("mouseover", function(e){
        configuracion.style.display = "block";
        volumen.style.display = "none";
    });

    volumen.addEventListener("mouseout", function(e){
        volumen.style.display = "none";
    });

    configuracion.addEventListener("mouseout", function(e){
        configuracion.style.display = "none";
    });

    reproductor.addEventListener("mousemove", function(e){
        cursor = true;
        controles.classList.remove("ocultar");
        controles.classList.add("mostrar");
    });

    subcapa.addEventListener("click", function(e){
        iniciardetener();
    });

    function accionesVideo() {
        if(video.ended){
            detener();
        }
        if(video.play){
            linea.value = video.currentTime;              
            linea.setAttribute("max",video.duration);
        }
        tiempo.innerHTML = "<span>"+segundosMinutos(video.currentTime)+" / "+segundosMinutos(video.duration)+"</span>";
    }

    function accionVideo() {
        if (!cursor) {
            controles.classList.remove("mostrar");
            controles.classList.add("ocultar");
            volumen.style.display = "none";
            configuracion.style.display = "none";
        } else {
            cursor=false;
        }
    }

