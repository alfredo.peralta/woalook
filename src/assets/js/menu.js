var labelSearch = document.getElementsByClassName("serach-words")[0];
var containerSearch = document.getElementsByClassName("search")[0];
var inputSearch = document.getElementById("search");
var buttonVoice = document.getElementsByClassName("btn-microfono")[0];
var voiceClick = false;
var buttonClear = document.getElementsByClassName("search-clear-button")[0];
const recognition = new webkitSpeechRecognition();
recognition.lang = 'es-ES'

inputSearch.addEventListener('keyup', function(e){
    if(e.code == "Backspace"){
        if(inputSearch.value == ""){
            labelSearch.style.display = "none";    
        }
    }

    if(inputSearch.value != null){
        if(inputSearch.value != ""){
            labelSearch.style.display = "block"; 
        }
    }else{
        labelSearch.style.display = "none";
    }
}); 

window.addEventListener('click', function(e){
    if (inputSearch.contains(e.target)){
        if(inputSearch.value != ""){
            labelSearch.style.display = "block"; 
        }
    } else{
        labelSearch.style.display = "none";
    }
});

buttonVoice.addEventListener('click', function(e){
    if(voiceClick){
        desactivarVoz();
        voiceClick = false;
    }else{
        activarVoz();
        voiceClick = true;
    }
});

buttonClear.addEventListener("click",function(e){
    inputSearch.value = "";
});

setInterval(function(){
    if(inputSearch.value == ""){
        buttonClear.style.display = "none";
    }else{
        buttonClear.style.display = "block";
    }
},800);

var js = document.querySelectorAll('script');

for (var i = 0; i < js.length; i++) {
    js[i].src = js[i].src + '?v=' + Date.now();
}

var css = document.querySelectorAll('link');

for (var i = 0; i < css.length; i++) {
    css[i].href = css[i].href + '?v=' + Date.now();
}

/** Voice **/
recognition.continuous = true
recognition.onresult = event => {
  for (const result of event.results) {
    inputSearch.value = result[0].transcript;
  }
}

function activarVoz(){
    try {
        recognition.start();   
    } catch (error) {
        recognition.stop();    
    }
    buttonVoice.classList.add("btn-microfono-animated");
}

function desactivarVoz(){
    recognition.stop();
    buttonVoice.classList.remove("btn-microfono-animated");
}

